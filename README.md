# Projet P1 2022-2023

## Membres
- LANGOUET Aymeric
- PERNET Gabriel
- VOLPATO Hugo
- HILL Zak

---
### [Sujet: Le 6 qui prend](http://idecourc.perso.cyu.fr/cours/INFO2/projet/2022-2023/projet22-23.html)
### [Rapport d'équipe](./rapport.pdf)
---

## Instructions
- Télécharger le makefile et le dossier src
- taper la commande make dans le terminal, situé a l'emplacement de son téléchargement
- taper la commande ./exe dans le terminal

## Notes
- Il y a 2 branches, la branche main et la branche UI++. La branche UI++ contient une tentative d'interface utilisateur non fonctionnelle et la branche main utilise de simples commandes printf() et devrait fonctionner sans problèmes. Les makefiles et certains fichiers sont ainsi différents, certaines fonctions ayant été retravaillées pour convenir au changement.
