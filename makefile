exe: manche.o joueur.o carte.o core.o main.o
	gcc main.o manche.o joueur.o carte.o core.o -o exe

manche.o: src/manche.c src/manche.h src/core.h
	gcc -c -Wall src/manche.c -o manche.o

joueur.o: src/joueur.c src/joueur.h src/core.h
	gcc -c -Wall src/joueur.c -o joueur.o

carte.o: src/carte.c src/carte.h src/core.h
	gcc -c -Wall src/carte.c -o carte.o

core.o: src/core.c src/core.h
	gcc -c -Wall src/core.c -o core.o

main.o: src/main.c src/core.h src/joueur.h src/manche.h
	gcc -c -Wall src/main.c -o main.o

#supprime les fichiers créés par l'exécution de la command make
clean:
	rm -f *.o
	rm -f exe
