#include "core.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(Carte* tab,int a,int b){
  Carte t = tab[a]; //var temporaire
  tab[a] = tab[b];
  tab[b] = t;
}

void clearBuffer() {
  char c;
  while ((c = getchar()) != '\n' && c != EOF);
}

int poserOuiNon(char* question) {
  char reponse;
  do {
    printf("%s (o/n)\n>>>", question); // pose la question
    reponse = getchar();
    if (reponse != 'o' && reponse != 'n') {
      printf("Erreur: reponse invalide.\n");
      clearBuffer();
    }
  } while (reponse != 'o' && reponse != 'n'); // tant que la réponse est invalide
  return reponse == 'o'; // retourne la réponse sous forme de booléen
}

int longueurChaine(char* chaine) {
  int i = 0;
  while (chaine[i] != '\0') {
    i++;
  }
  return i;
}

void rognerChaine(char* chaine) {
  int longueur = longueurChaine(chaine);
  char* res = malloc(sizeof(char) * longueur);
  int ideb = 0; 
  //rogner le début
  while (chaine[ideb] == ' ' || chaine[ideb] == '\n') {
    ideb++;
  }
  int ifin = longueur - 1; 
  //rogner la fin
  while (chaine[ifin] == ' ' || chaine[ifin] == '\n') {
    ifin--;
  }
  for (int i = ideb; i <= ifin; i++) { // création de la nouvelle chaine
    res[i - ideb] = chaine[i];
  }
  strcpy(chaine, res); //changement de la chaine originale
  free(res);
}

int fin(TabJoueurs TabJ){
  int perdu = 0; //booléen
  int i = 0;
  while(!perdu && i<TabJ.nb_joueurs){
    if(TabJ.joueurs[i].points>66){
      perdu = 1;
    }
    i++;
  }
  return perdu; //renvoie 1 si quelqu'un a perdu, 0 sinon
}
