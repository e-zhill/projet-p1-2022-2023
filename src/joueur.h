#ifndef __joueur_H_
#define __joueur_H_
#include "core.h"

/* Auteur : Volpato Hugo */
/* Date :  12/06/2023 */
/* fonction qui crée un joueur */
/* préconditions: none */
/* postconditions: renvoie un variable de type Joueur* */
Joueur* creationJoueur(int place);

/* Auteur : Volpato Hugo & Pernet Gabriel */
/* Date :  12/06/2023 */
/* Fonction de tri rapide qui ordonne par ordre croissant les valeurs des cartes de sa main */
/* précondition: la main d'un joueur */
/* postcondition: none */
void ordonnerCartes(Carte* main, int debut, int fin);

/* Auteur : Volpato Hugo */
/* Date :  13/06/2023 */
/* Fonction qui trouve l'indice d'une carte qui se trouve dans la main du joueur */
/* précondition: la main du joueur (pointeur de Carte), la valeur de la carte cherchee, et les indices du début et de fin de la main (des entiers) */
/* postcondition: la fonction renvoie un entier */
int rechercheDichotomique(Carte* main, int valeur, int debut, int fin);

/* Auteur : Volpato Hugo */
/* Date :  13/06/2023 */
/* Dans cette fonction, le joueur choisit la carte qu'il va jouer */
/* précondition: la main du joueur, de type Carte*, est passée en paramètre */
/* postcondition: la fonction renvoie la carte choisie par le joueur, variable de type Carte* */
void choixCarte(Joueur* joueur, int nb_tour);

/* Auteur : Volpato Hugo */
/* Date :  13/06/2023 */
/* Fonction qui compte le nombre de points d'un joueur à la fin de la manche */
/* préconditions: la fonction prend en paramètre la pile du joueur, variable de type Carte* */
/* postcondition: la fonction renvoie une variable de type entier */
int compterPoints(Pile* pile);

#endif
