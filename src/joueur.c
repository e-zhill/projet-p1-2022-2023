#include <stdio.h>
#include <stdlib.h>
#include "joueur.h"
#include "core.h"

Joueur* creationJoueur(int place){
  Joueur* j;
  j = malloc(sizeof(Joueur));

  printf("Joueur %d, entrez votre nom (30 caractères maximum):\n>>>",place+1);
  fgets(j->nom,sizeof(char)*30,stdin);

  rognerChaine(j->nom); 
  // supprime les espaces et les retours à la ligne au début et à la fin du nom du joueur

  j->points = 0;

  j->pile = malloc(sizeof(Pile));
  j->pile->tab = malloc(50* sizeof(Carte));
  
  j->position = place;

  return j;
}

void ordonnerCartes(Carte* main, int debut, int fin) // tri rapide
{ 
  // pivot
  int pivot = debut; 
  // avancement du tri 
  int adv_tri = debut;
  // fin si tableau de dimension 1
  if (debut != fin){
  // si le tri n'est pas fini
    while (adv_tri < fin){ 
      if (main[adv_tri+1].valeur < main[pivot].valeur){
        swap(main, adv_tri+1, pivot+1); 
        // échange intermédiaire pour que le pivot avance aussi.
        swap(main, pivot, pivot+1);     
        // la valeur se retrouve don derrière le pivot
        if (pivot+1 < fin){
          pivot++;
        }
      }
      adv_tri++;
    }
  ordonnerCartes(main, debut, pivot); 
  // tri de ce qui est a gauche du pivot
  ordonnerCartes(main, pivot+1, fin); 
  // tri de ce qui est a droite du pivot
  }
}

int rechercheDichotomique(Carte* main, int valeur, int debut, int fin){
  if (debut <= fin) {
    int milieu = (debut + fin) /2;                                 
    //récursivité jusqu'à ce que la valeur soit trouvée
    if (main[milieu].valeur > valeur){
      return rechercheDichotomique(main, valeur, debut, milieu-1); 

    } else if (main[milieu].valeur < valeur) {
      return rechercheDichotomique(main, valeur, milieu+1, fin);

    } else {
      return milieu;
    }
  } else {
    return -1;
  }
}

void updateMain(Carte* main,int indice_carte,int fin){
  // place la carte choisie à la fin de la main du joueur
  for(int i=indice_carte;i<fin;i++){
    swap(main,i,i+1);
  }
}

void choixCarte(Joueur* joueur, int nb_tour)
{
  int indice_carte = -1;
  int valeur;
  do {
    printf("%s, écrivez la valeur de la carte que vous souhaitez jouer : \n", joueur->nom);
    scanf("%d",&valeur);
    indice_carte = rechercheDichotomique(joueur->main, valeur, 0, 9-nb_tour);  
    // cherche la carte saisie dans la main du joueur
  } while (indice_carte ==-1);

  joueur->carteChoisie = joueur->main[indice_carte];  
  //enregistre la carte comme étant choisie pour le tour
  updateMain(joueur->main,indice_carte,9-nb_tour); 
}

int compterPoints(Pile* pile){
  int somme = 0;
  int N = pile->taille; 
  // fait la somme des têtes de boeuf de toutes les cartes
  for (int i=0; i<N; i++){
    somme += pile->tab[i].nbTdB;
  }
  return somme;
}


