#include "manche.h"
#include "core.h"
#include "joueur.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void affmain(Joueur* joueur,int taille){
  printf("%s, voici votre main\n",joueur->nom);
  printf("Valeur des cartes : ");
  for(int i=0;i<taille;i++){
    printf("%d ",joueur->main[i].valeur);
  }
  printf("\nNb Têtes de boeuf : ");
  for(int i=0;i<taille;i++){
    printf("%d ",joueur->main[i].nbTdB);
  }
  printf("\n\n"); // aère l'affichage
}

void affRangees(Pile* rangees){
  printf("affichage des rangées\n");
  for(int i=0;i<4;i++){
    printf("- rangée n°%d : ",i+1);
    for(int j=0;j<rangees[i].taille;j++){
      printf("%d ",rangees[i].tab[j].valeur);
    }
    rangees[i].total = compterPoints(&rangees[i]);
    printf(" ;%d têtes de boeuf\n",rangees[i].total); 
    //affiche la somme des têtes de boeuf des cartes de la rangée
  }
  printf("\n");  // aère l'affichage
}

void choixRangee(Joueur* joueur, Pile* rangees){
  int num = 0;
  while(num<1||num>4){
    affRangees(rangees);
    printf("%s, votre carte est trop faible pour être jouée. Veulliez choisir la rangée a mettre dans votre pile.\n",joueur->nom);
    scanf("%d",&num);
  }
  encaisserRangee(joueur,rangees,num - 1);  
  //cartes de la rangée déplacées dans la pile du joueur
}

void formationRangees(Pioche* pioche, Pile* rangees) {
  for (int i = 0; i < 4; i++) { 
    // initialisation des rangées avec les quatre premières cartes de la pioche
    rangees[i].tab[0] = pioche->paquet[pioche->debut];
    rangees[i].taille = 1;
    pioche->debut++;
  }
}

int deposerCarte(Joueur* joueur, Pile* rangees) {
  int difference;
  int min = 105;
  int imin;
  int rangeeTrouvee = 0;
  printf("tour de %s\n",joueur->nom);
  sleep(1);
  for (int i = 0; i < 4; i++) {
    // vérifie si la carte peut être posée normalement
    difference = joueur->carteChoisie.valeur - rangees[i].tab[rangees[i].taille-1].valeur;
    if (difference > 0 && difference < min) { 
      min = difference;
      imin = i;
      rangeeTrouvee = 1;
    }
  }
  // cas ou la carte est posée normalement
  if (rangeeTrouvee) {
    if (rangees[imin].taille >= 5) { //si la rangée est pleine
      encaisserRangee(joueur, rangees, imin);
    } else { 
      // place la carte du joueur a la suite de la rangée
      rangees[imin].tab[rangees[imin].taille] = joueur->carteChoisie;
      rangees[imin].taille++;
      printf("Carte de %s déposée dans la rangée n°%d\n", joueur->nom, imin + 1);
    }
  }
  return rangeeTrouvee;
}

void encaisserRangee(Joueur* joueur, Pile* rangees, int rangee) {
  for (int i = 0; i < rangees[rangee].taille; i++) {
    joueur->pile->tab[joueur->pile->taille] = rangees[rangee].tab[i];
    joueur->pile->taille++;
  } 
  // place la carte du joueur comme nouveau dépatrt de la rangée
  rangees[rangee].tab[0] = joueur->carteChoisie;
  rangees[rangee].taille = 1;
  printf("%s encaisse la rangée n°%d\n",joueur->nom,rangee+1); //informe l'utilisateur
}

void defOrdreTour(TabJoueurs tabJ,int* tabPosit){
   //tableau des cartes choisies
  Carte* tabChoisies = malloc(tabJ.nb_joueurs*sizeof(Carte));
  int ok;
  int j;
  for(int i=0;i<tabJ.nb_joueurs;i++){
    tabChoisies[i]=tabJ.joueurs[i].carteChoisie;
  }
  // classe les cartes dans l'ordre croissant
  ordonnerCartes(tabChoisies,0,tabJ.nb_joueurs - 1); 
  // détermine la position des joueurs selon celle de leur carte
  for(int i=0;i<tabJ.nb_joueurs;i++){
    j=0;
    ok=0;
    while(!ok){
      if(tabChoisies[i].valeur==tabJ.joueurs[j].carteChoisie.valeur){
        tabPosit[i]=tabJ.joueurs[j].position;
        ok=1;
      }
      j++;
    }
  }
  printf("Valeur des cartes choisies selon l'ordre du tour :\n");
  for(int i=0;i<tabJ.nb_joueurs;i++){
    printf("%d : %s avec %d\n",i+1,tabJ.joueurs[tabPosit[i]].nom,tabJ.joueurs[tabPosit[i]].carteChoisie.valeur);
  }
  free(tabChoisies);
  tabChoisies=NULL;
}

void jouerTour(Pile* rangees, TabJoueurs tabJ, int nb_tour){
  // tableau déterminant l'ordre du tour, utilisant la position des joueurs dans le tableau de joueurs
  int* tabPosit = malloc(tabJ.nb_joueurs*sizeof(int));
  printf("Début du tour %d\n\n",nb_tour+1);
  sleep(1);
  // choix des cartes
  for(int i=0;i<tabJ.nb_joueurs;i++){
    affRangees(rangees);
    affmain(&tabJ.joueurs[i],10-nb_tour);
    choixCarte(&tabJ.joueurs[i],nb_tour);
    system("clear");
  }
  defOrdreTour(tabJ,tabPosit);
  // pose des cartes
  for(int i=0;i<tabJ.nb_joueurs;i++){
    printf("\n");
    if(!deposerCarte(&tabJ.joueurs[tabPosit[i]],rangees)){
      choixRangee(&tabJ.joueurs[tabPosit[i]],rangees);
    }
    sleep(1);
  }
  sleep(1);
  system("clear");
  free(tabPosit);
  tabPosit=NULL;
}

void jouerManche(TabJoueurs tabJ){
  // initialisation
  Pioche* pioche = creerPioche();
  Pile* rangees = malloc(4*sizeof(Pile));
  for(int i=0;i<4;i++){
    rangees[i].tab=malloc(5*sizeof(Carte));
  }
  shuffle(pioche);
  formationRangees(pioche,rangees);
  for(int i=0;i<tabJ.nb_joueurs;i++){
    distrib(pioche,&(tabJ.joueurs[i]));
    ordonnerCartes(tabJ.joueurs[i].main, 0, 9);
  }
  free(pioche);
  pioche=NULL;
  // joue 10 tours
  for(int i=0;i<10;i++){
    jouerTour(rangees,tabJ,i);
  }
  for(int i=0;i<4;i++){
    free(rangees[i].tab);
    rangees[i].tab=NULL;
  }
  free(rangees);
  rangees = NULL;
  // compte les points reçus pendant la manche
  for(int i=0;i<tabJ.nb_joueurs;i++){
    tabJ.joueurs[i].pile->total=compterPoints(tabJ.joueurs[i].pile);
    tabJ.joueurs[i].points+=tabJ.joueurs[i].pile->total;
  }
}
