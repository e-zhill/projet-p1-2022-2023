
#include "core.h"
#include "joueur.h"
#include "carte.h"

#ifndef __manche_H_
#define __manche_H_

// Auteur: Pernet Gabriel & Volpato Hugo
// Date : 15/06/2023
// Affiche la main d'un joueur
// Preconditions: la main du joueur et la taille de cette main
// Postconditions: none
void affmain(Joueur* joueur,int taille);

// Auteur: Pernet Gabriel & Volpato Hugo
// Date : 15/06/2023
// Affiche les rangées
// Preconditions: les rangees
// Postconditions: none
void affRangees(Pile* rangees);

// Auteur: Zak Hill
// Date : 16/06/2023
// Forme les rangees a partir de la pioche.
// Preconditions: la pioche et le tableau des rangees vide
// Postconditions: 
void formationRangees(Pioche* pioche, Pile* rangees);

// Auteur: Pernet Gabriel
// Date : 16/06/2023
// Demande au joueur de choisir une rangee.
// Preconditions: un joueur et les rangees
// Postconditions: un entier entre 0 et 3 inclus representant l'index de la rangee
void choixRangee(Joueur* joueur, Pile* rangees);

// Auteur: Zak Hill
// Date : 16/06/2023
// Tente de deposer une carte dans une des rangees.
// Preconditions: une carte et les rangees
// Postconditions: 1 si la carte a pu etre deposee, 0 sinon
int deposerCarte(Joueur* joueur, Pile* rangees);

// Auteur: Zak Hill
// Date : 16/06/2023
// Encaisse la rangee selectionnee pour le joueur
// Preconditions: le joueur, les rangees et l'index d'une rangee
// Postconditions: 
void encaisserRangee(Joueur* joueur, Pile* rangees, int rangee);

// Auteur: Pernet Gabriel
// Date : 16/06/2023
// Détermine l'ordre du tour
// Preconditions: les joueurs et combien ils sont
// Postconditions: renvoie un tableau d'entiers qui correspond aux positions des joueurs dans leur tableau rangées dans l'ordre du tour
void defOrdreTour(TabJoueurs tabJ,int* tabPosit);

// Auteur: 
// Joue un tour: Pernet Gabriel & Volpato Hugo & LANGOUET Aymeric
//   - demande aux joueurs de choisir une carte
//   - depose les cartes choisie dans les rangees suivant l'ordre croissant
// Preconditions: les rangees, les joueurs et le numéro du tour -1
// Postconditions: 
void jouerTour(Pile* rangees, TabJoueurs tabJ, int nb_tour);

// Auteur: 
// Joue une manche: Pernet Gabriel & Volpato Hugo & LANGOUET Aymeric
//   - cree et melange la pioche
//   - distribue les cartes aux joueurs
//   - forme les rangees
//   - joue les 10 tours de la manche
//   - compte et attribue les points a la fin de la manche
// Preconditions: les joueurs
// Postconditions: 
void jouerManche(TabJoueurs tabJ);

#endif