
#include "core.h"
#include "joueur.h"
#include "manche.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


// Auteur: 
// Gere la boucle principale du jeu
int main(int argc, char** argv) {

  srand(time(NULL));
  system("clear");

  printf("  ,--.      ,-----.   ,--. ,--.,--.    ,------. ,------. ,------.,--.  ,--.,------.  \n /  .'     '  .-.  '  |  | |  ||  |    |  .--. '|  .--. '|  .---'|  ,'.|  ||  .-.  \\ \n|  .-.     |  | |  |  |  | |  ||  |    |  '--' ||  '--'.'|  `--, |  |' '  ||  |  \\  :\n\\   o |    '  '-'  '-.'  '-'  '|  |    |  | --' |  |\\  \\ |  `---.|  | `   ||  '--'  /\n `---'      `-----'--' `-----' `--'    `--'     `--' '--'`------'`--'  `--'`-------' \n");
  printf("\nRegles du jeu: https://www.regledujeu.fr/6-qui-prend/\n");
  // printf("Regles du jeu: \e]8;;https://www.youtube.com/watch?v=dQw4w9WgXcQ\e\\https://www.regledujeu.fr/6-qui-prend/\e]8;;\e\\\n");

  TabJoueurs tabJoueurs;
  tabJoueurs.nb_joueurs = 0;
  int vainqueur = 0;

// création des joueurs
  while (1) {
    tabJoueurs.joueurs = tabJoueurs.nb_joueurs == 0
      ? malloc(sizeof(Joueur))
      : realloc(tabJoueurs.joueurs, sizeof(Joueur) * (tabJoueurs.nb_joueurs + 1));
    tabJoueurs.joueurs[tabJoueurs.nb_joueurs] = *creationJoueur(tabJoueurs.nb_joueurs);
    tabJoueurs.nb_joueurs++;
    if (tabJoueurs.nb_joueurs >= 10 || !poserOuiNon("Ajouter un autre joueur ?")) {
      if(tabJoueurs.nb_joueurs >= 10){
        printf("nombre maximum de joueurs atteint\n");
      }
      break;
    }
    clearBuffer();
  }
  system("clear");

  // lancement de la partie, boucle jusqu'a ce que quelqu'un perdes(+ de 66 têtes de boeuf)
  while(!fin(tabJoueurs)){
    printf("Début de manche\n\n");
    sleep(1);
    // lance une manche
    jouerManche(tabJoueurs);
    // affiche les points des joueurs
    printf("Fin de manche, affichage des points\n");
    for(int i=0;i<tabJoueurs.nb_joueurs;i++){
      printf("%s : %d points\n",tabJoueurs.joueurs[i].nom,tabJoueurs.joueurs[i].points);
    }
    sleep(5);
    system("clear");
  }
  // déterminer le vainqueur
  for(int i=1;i<tabJoueurs.nb_joueurs;i++){
    if(tabJoueurs.joueurs[i].points<tabJoueurs.joueurs[vainqueur].points){
      vainqueur=tabJoueurs.joueurs[i].position;
    }
  }
  // annonce des points et du vainqueur
  for(int i=0;i<tabJoueurs.nb_joueurs;i++){
    printf("%s : %d points\n",tabJoueurs.joueurs[i].nom,tabJoueurs.joueurs[i].points);
  }
  printf("fin de la partie, %s a gagné\n",tabJoueurs.joueurs[vainqueur].nom);

  free(tabJoueurs.joueurs);
  return 0;
}
