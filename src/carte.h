
#ifndef __carte_H_
#define __carte_H_
#include "core.h"

// Auteur : Pernet Gabriel
// Date :  12/06/2023
// Fonction qui compte le nombre de têtes de boeuf d'une carte selon sa valeur
// préconditions: la fonction prend en paramètre une variable de type entier
// postcondition: la fonction renvoie une variable de type entier
int calculTdB(int val);

// Auteur : Pernet Gabriel
// Date :  12/06/2023
// Fonction qui crée la pioche
// préconditions: none
// postcondition: la fonction renvoie un pointeur de pioche, elle même composée d'un début = 0 et d'un paquet contenant les cartes de valeurs 1 à 104
Pioche* creerPioche();

// Auteur : Pernet Gabriel
// Date :  12/06/2023
// Fonction qui mélange la pioche
// préconditions: la fonction prend en paramètre la pioche
// postcondition: none
void shuffle(Pioche* pioche);

// Auteur : Pernet Gabriel
// Date :  12/06/2023
// Fonction qui distribue un main de 10 cartes à un joueur
// préconditions: la fonction prend en paramètre la pioche et un joueur
// postcondition: none
void distrib(Pioche* pioche,Joueur* joueur);

#endif