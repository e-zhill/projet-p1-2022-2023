#include "carte.h"
#include "core.h"
#include <stdlib.h>

int calculTdB(int val){  // nombre de têtes de boeuf selon la carte
  int res;
  if(val==55){  // 7 si 55
    res = 7;
  }else if(val%11==0){ // 5 si double
    res = 5;
  }else if(val%10==0){ // 3 si finit par 0
    res = 3;
  }else if(val%5==0){  // 2 si finit par 5
    res = 2;
  }else{               // 1 sinon
    res = 1;
  }
  return res;
}

Pioche* creerPioche(){
  Pioche* pioche = malloc(sizeof(Pioche));
  pioche->debut = 0;
  pioche->paquet = malloc(104*sizeof(Carte));    
  // initialise les valeurs et nombres de têtes de boeuf des cartes dans la pioche
  for(int i=0;i<104;i++){ 
    pioche->paquet[i].valeur = i+1;
    pioche->paquet[i].nbTdB = calculTdB(pioche->paquet[i].valeur);
  }
  return pioche;
}

void shuffle(Pioche* pioche){
  // mélange la pioche en échangeant les places des cartes
  for(int i=0;i<104;i++){
    swap(pioche->paquet,i,rand()%104);  
  }
}

void distrib(Pioche* pioche,Joueur* joueur){
  // 10 fois de suite, attribue à la ième carte de la main du joueur la première carte de la pioche
  for(int i=0;i<10;i++){
    joueur->main[i]=pioche->paquet[pioche->debut]; 
    pioche->debut++;
  }
}