#ifndef __core_H_
#define __core_H_

typedef struct {
  int valeur; // valeur numérique de la carte
  int nbTdB;  // nombre de têtes de boeuf sur la carte
}Carte;

typedef struct {
  int debut;      // position de la 1ère carte de la pioche
  Carte* paquet;  // tableau contenant les cartes de 1 à 104
}Pioche;


typedef struct {
  int taille; // nombre de cartes dans la pile
  Carte* tab; // tableau contenant les cartes qui sont dans la pile
  int total;  // sommes des têtes de boeuf des cartes
} Pile;

typedef struct {
  char nom[30];       // nom du joueur
  int points;         // points du joueur
  Carte main[10];     // tableau contenant les cartes de la main du joueur
  Carte carteChoisie; // plus récent choix de carte du joueur
  Pile* pile;         // pile contenant les cartes ramassées pendant la manche
  int position;       // position du joueur dans le tableau qui contient tous les joueurs. 1er jouer = 0, 2eme = 1 ...
} Joueur;

typedef struct {
  int nb_joueurs;   // nombre total de joueurs dans la partie
  Joueur* joueurs;  // tableau contenant tous les joueurs
} TabJoueurs;

// Auteur : Pernet Gabriel
// Date :  12/06/2023
// Fonction qui échange 2 cartes d'un tableau de cartes
// préconditions: la fonction prend en paramètre un tableau de carte et 2 valeurs entières, les indices du tableau qui seront échangés
// postcondition: none
void swap(Carte* tab,int a,int b);

// Auteur: Peio Loubière
// Vide le buffer après une saisie
// preconditions: 
// postcondition: 
void clearBuffer();

// Auteur: Zak Hill
// Pose une question fermee repondue par oui/non
// preconditions: une question
// postconditions: 1 si oui, 0 si non
int poserOuiNon(char* question);

// Auteur: Zak Hill
// Donne la longueur d'une chaine
// preconditions: une chaine
// postconditions: la longueur de la chaine
int longueurChaine(char* chaine);

// Auteur: Zak Hill
// Rogne une chaine (enleves les espaces et retours a la ligne au debut et a la fin)
// preconditions: une chaine (qui sera free a la fin)
// postconditions: la chaine rognee
void rognerChaine(char* chaine);

// Auteur : Pernet Gabriel
// Date :  19/06/2023
// Fonction qui regarde si la partie est finie
// préconditions: la fonction prend en paramètre le tableau des joueurs et combien ils sont
// postcondition: renvoie un bolléen, 1 si la partie est finie, 0 sinon
int fin(TabJoueurs TabJ);

#endif
